Django eShop on OpenShift
=
This git repository helps you get up and running quickly with django v1.7+ and Openshift.
###Features
* Ready to use for local development
* Easy to push to Openshift
* Works with  MySQL
* Minimal changes to default django installation
* Names follow the django tutorial
* Uses new folder layout from Openshift March 2014 release
* Allow for debug mode on Openshift with the help of an environment variable.
* Use of static files is pre-configured

###How to use this repository
- Create an account at https://www.openshift.com
- Install the RHC client tools if you have not already done so.
```
sudo gem install rhc
rhc setup
```
- Create a Python 2.7 application and add MYSQL
```
rhc app create eshop python-2.7
rhc add-cartridge mysql-5.5 --app eshop
```
```
```
- cd eshop
```
git remote add upstream -m master https://christophesurbier@bitbucket.org/christophesurbier/eshop.git
git pull -s recursive -X theirs upstream master
```
- set the WSGI application to django's built in WSGI application (stored in the wsgi folder).
```
rhc env set OPENSHIFT_PYTHON_WSGI_APPLICATION=wsgi/wsgi.py --app eshop
```
- Push the repo upstream
```
git push
```
- SSH into the application to create a django superuser.
```
rhc ssh -a eshop
python app-root/repo/manage.py createsuperuser
```
- Now use your browser to connect to the Admin site : http://<your domain name>/django/admin

