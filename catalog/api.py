from models import Category,Product,ProductImage
from tastypie import fields
from tastypie.resources import ModelResource, ALL_WITH_RELATIONS
from tastypie.authorization import DjangoAuthorization
from authentication import OAuth20Authentication

class MultiPartResource(object):
    def deserialize(self, request, data, format=None):
        if not format:
            format = request.Meta.get('CONTENT_TYPE', 'application/json')
        if format == 'application/x-www-form-urlencoded':
            return request.POST
        if format.startswith('multipart'):
            data = request.PUT.copy()
            data.update(request.FILES)
            return data
        return super(MultiPartResource, self).deserialize(request, data, format)


class CategoryResource(ModelResource):
    refParent = fields.ForeignKey('catalog.api.CategoryResource', 'refParent', full=False, null=True)
    class Meta:
        queryset = Category.objects.all()
        resource_name = 'category'
        collection_name = 'category'
        authorization = DjangoAuthorization()
        authentication = OAuth20Authentication()

        filtering = {
            'refParent': ['exact'],
        }

class ProductResource(ModelResource):
    categoryId = fields.ForeignKey(CategoryResource, 'categoryId')
    class Meta:
        queryset = Product.objects.all()
        resource_name = 'product'
        collection_name = 'product'
        authorization = DjangoAuthorization()
        authentication = OAuth20Authentication()
        filtering = {
            'categoryId': ['exact'],
            'title': ALL_WITH_RELATIONS,
            'published': ['exact'],
        }

class ProductImageResource(ModelResource):
    productId = fields.ForeignKey(ProductResource, 'productId')
    class Meta:
        queryset = ProductImage.objects.all()
        resource_name = 'productimage'
        collection_name = 'productimage'
        authorization = DjangoAuthorization()
        authentication = OAuth20Authentication()
        filtering = {
            'productId': ['exact'],
        }

