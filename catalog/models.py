from __future__ import unicode_literals

from django.db import models
from mysite import settings

# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=2000)
    image = models.ImageField(upload_to='media')
    refParent = models.ForeignKey('Category',null=True,blank=True,default=None)
    tags = models.CharField(max_length=20)
    createdAt = models.DateTimeField(auto_now=True)
    updatedAt = models.DateTimeField(auto_now=True)
    class Meta:
        unique_together = (
            ('name', 'refParent')
            )   

    def imagePreview(self):
        return """<img src="%s" width="110" height="135"/>""" % (settings.MEDIA_URL+self.image.name)

    imagePreview.allow_tags = True

    def __unicode__(self):
        return u'%s' % (self.name)

class Product(models.Model):
    categoryId = models.ForeignKey('Category')
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=2000)
    mainImage = models.ImageField(upload_to='media')
    published = models.BooleanField()
    price = models.FloatField()
    sales_price = models.FloatField(null=True,blank=True)
    sales_price_from = models.DateTimeField(null=True,blank=True)
    sales_price_to = models.DateTimeField(null=True,blank=True)
    size_choice = (
        (0, "XS"),
        (1, "S"),
        (2, "M"),
        (3, "L"),
        (4, "XL"),
        (5, "XXL"),
    )
    size = models.IntegerField(choices=size_choice,null=True,blank=True)
    # Add any color choice available in your catalog here
    color_choice = (
        (0, "White"),
        (1, "Black"),
        (2, "Gray"),
        (3, "Blue"),
        (4, "Red"),
    )
    color = models.IntegerField(choices=color_choice,null=True,blank=True)
    stock_quantity = models.IntegerField()
    in_stock = models.BooleanField() # will be updated automatically when stock_quantity =0
    featured = models.BooleanField() # Put your product in featured option on your front
    dimension = models.CharField(max_length=1000,null=True,blank=True) # Describe your product dimension (width,height, lenght...) here
    createdAt = models.DateTimeField(auto_now=True)
    updatedAt = models.DateTimeField(auto_now=True)

    def mainImagePreview(self):
        return """<img src="%s" width="110" height="135"/>""" % (settings.MEDIA_URL+self.mainImage.name)

    mainImagePreview.allow_tags = True

    def __unicode__(self):
        return u'%s - %s' % (self.categoryId,self.title)


class ProductImage(models.Model):
    productId = models.ForeignKey('Product')
    image = models.ImageField(upload_to='media')

    def imagePreview(self):
        return """<img src="%s" width="110" height="135"/>""" % (settings.MEDIA_URL+self.image.name)

    imagePreview.allow_tags = True

    def __unicode__(self):
        return u'%s - %s' % (self.productId)