from django.contrib import admin
from models import Category
from models import Product,ProductImage

class CategoryAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['name', 'imagePreview','description','refParent','tags']}),
    ]
    list_display = ('name', 'imagePreview','description','refParent','tags',)
    list_filter = ('refParent',)
    ordering = ('name', 'refParent',)
    search_fields = ('name',)

class ProductAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['categoryId', 'title', 'description', 'mainImagePreview','published','price','sales_price','sales_price_from','sales_price_to','size','color','stock_quantity','in_stock','dimension','featured']}),
    ]
    list_display = ('categoryId', 'title', 'description', 'mainImagePreview','published','price','sales_price','sales_price_from','sales_price_to','size','color','stock_quantity','in_stock','dimension','featured',)
    list_filter = ('categoryId', 'published','featured','in_stock','title','stock_quantity',)
    ordering = ('categoryId', 'title','published',)
    search_fields = ('title','description',)

class ProductImageAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['productId', 'imagePreview']}),
    ]
    list_display = ('productId', 'imagePreview',)
    list_filter = ('productId',)
    ordering = ('productId',)
    search_fields = ('productId',)


admin.site.register(Category,ProductAdmin)
admin.site.register(Product,ProductAdmin)
admin.site.register(ProductImage,ProductImageAdmin)